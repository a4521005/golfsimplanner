import time
from selenium.webdriver import Chrome

def create_chrome_driver() -> Chrome:
    return Chrome()

def request_ptt_boards(driver:Chrome):
    url="https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=7%278%22x7%278%22%20&launch_monitor_position=Top&recommended_projector=LW600ST"
    driver.get(url)
    time.sleep(30)


if __name__=='__main__':
    chrome_driver=create_chrome_driver()
    request_ptt_boards(driver=chrome_driver)
    chrome_driver.quit()