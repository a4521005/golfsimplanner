import pytest
import requests
from fake_useragent import FakeUserAgent

@pytest.fixture(name="headers")
def headers_fixture() -> dict:
    fake_useragent = FakeUserAgent()

    return {"user-agent": fake_useragent.chrome}

def test_status_code(headers: dict):
    url = "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=7%278%22x7%278%22%20&launch_monitor_position=Top&recommended_projector=LW600ST"

    print(headers)
    res=requests.get(url=url, headers=headers)
    print(res.status_code)
    if res.status_code==200:
        print("Pass")
    else:
        print("Fail")