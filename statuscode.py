# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 13:55:01 2023

@author: Celine.HY.Chiu
"""

import requests
import json
import pandas as pd

allURL=[
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%27x8%27 &launch_monitor_position=Side&recommended_projector=LH600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%27x8%27 &launch_monitor_position=Back&recommended_projector=LW600ST"
  },
  {
    "Model": "LW500ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%27x8%27 &launch_monitor_position=Top&recommended_projector=LH600ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%27x8%27 &launch_monitor_position=Side&recommended_projector=LH600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Top&recommended_projector=LH600ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Top&recommended_projector=LH600ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Back&recommended_projector=LW600ST"
  },
  {
    "Model": "LW500ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=8%276\"x8%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Top&recommended_projector=LW600ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Top&recommended_projector=LW600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Back&recommended_projector=LW600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%27x9%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=9%276”x9%276” &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LW600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=10%27x10%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=7%278”x10%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%27x10%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Top&recommended_projector=LU710"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=8%276\"x11%276\" &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Top&recommended_projector=LU710"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%27x12%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU710",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=9%276\"x12%276\" &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU710",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=4%3A3&screen_size__golf_=10%27x13%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=7%278\"x11%276\" &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Top&recommended_projector=LH600ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Back&recommended_projector=LH600ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Top&recommended_projector=LH600ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%27x12%27 &launch_monitor_position=Side&recommended_projector=LH600ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=8%276\"x13%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH600ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%27x14%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=9%276\"x15%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A10&screen_size__golf_=10%27x15%276\" &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Top&recommended_projector=TK700STi"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Back&recommended_projector=TK700STi"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Top&recommended_projector=TK700STi"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Back&recommended_projector=TK700STi"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Top&recommended_projector=TK700STi"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=7%278\"x13%27 &launch_monitor_position=Side&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "TK700STi",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "TK700STi",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Top&recommended_projector=LU710"
  },
  {
    "Model": "TK700STi",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%27x14%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "TK700STi",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "TK700STi",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Top&recommended_projector=LU710"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=8%276\"x15%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU710",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Top&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Back&recommended_projector=LH820ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Top&recommended_projector=LU710"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%27x16%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU710",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Side&recommended_projector=LU935ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Back&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Top&recommended_projector=LU935ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=9%276\"x17%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LH820ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU710",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LU935ST",
    "url": ""
  },
  {
    "Model": "LU935ST",
    "url": ""
  },
  {
    "Model": "LU935ST",
    "url": ""
  },
  {
    "Model": "LK936ST",
    "url": ""
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Back&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Top&recommended_projector=LK936ST"
  },
  {
    "Model": "LK936ST",
    "url": "https://golfsimplanner-stage.benq.com/?aspect_ratio=16%3A9&screen_size__golf_=10%27x18%27 &launch_monitor_position=Side&recommended_projector=LK936ST"
  }
]


