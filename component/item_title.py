from contextlib import contextmanager
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
import time

@contextmanager
def create_chrome_driver() -> Chrome:
    driver=Chrome()
    yield driver
    driver.quit()

def get_golf_by_css(driver:Chrome):
    url="https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=7%278%22x7%278%22%20&launch_monitor_position=Top&recommended_projector=LW600ST"
    driver.get(url)
    time.sleep(60)

    #v-overlay__scrim
    css_string="div.item-title"

    # 尋找item-title
    item_title_element=driver.find_element(
        By.CSS_SELECTOR, css_string
    )
    print(item_title_element.text)

if __name__=='__main__':
    with create_chrome_driver() as chrome_driver:
        get_golf_by_css(driver=chrome_driver)
        chrome_driver.quit()