from contextlib import contextmanager
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

@contextmanager
def create_chrome_driver() -> Chrome:
    driver=Chrome()
    yield driver
    driver.quit()


#get web title and url
def get_basic_page_message(driver:Chrome):
    url="https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=7%278%22x7%278%22%20&launch_monitor_position=Top&recommended_projector=LW600ST"
    driver.get(url)
    time.sleep(60)

    print(driver.title)
    print(driver.current_url)

#click
def demo_click_element(driver: Chrome):
    url = "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=7%278%22x7%278%22%20&launch_monitor_position=Top&recommended_projector=LW600ST"
    driver.get(url)
    time.sleep(60)

    i_understand_button="body > div.v-overlay-container > div.v-overlay.v-overlay--active.v-theme--light.v-locale--is-ltr.v-dialog.cookie-dialog > div.v-overlay__content > div > div.v-card-text.cookie-content > div.cookie-button-link-container > button"
    understand_button=driver.find_element(
        By.CSS_SELECTOR, i_understand_button
    )
    driver.execute_script(
        "arguments[0].click();",
        understand_button
    )
    time.sleep(10)

    close_button="body > div.v-overlay-container > div:nth-child(16) > div.v-overlay__content > div > div.v-card-text > div.flex.justify-end > i"
    x_button=driver.find_element(
        By.CSS_SELECTOR, close_button
    )

    # 內建元素
    # i_understand_button.click()


    #Javascript元素點擊
    driver.execute_script(
        "arguments[0].click();",
        x_button
    )
    time.sleep(5)

#scroll page
def demo_scroll_page(driver: Chrome):
    url = "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=7%278%22x7%278%22%20&launch_monitor_position=Top&recommended_projector=LW600ST"
    driver.get(url)
    time.sleep(60)

    driver.execute_script(
        "window.scrollTo(0, document.body.scrollHeight);",
    )
    time.sleep(5)

#keyboard(輸入文字,按Enter)
def demo_keyboard_action(driver: Chrome):
    url="https://www.google.com.tw/?hl=zh_TW"
    driver.get(url)

    search_bar=driver.find_element(
        By.XPATH, "/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/textarea"
    )

    search_text="HAHOW"
    for i in search_text:
        search_bar.send_keys(i)
        time.sleep(1)

    search_bar.send_keys(Keys.ENTER)
    time.sleep(10)



if __name__=='__main__':
    with create_chrome_driver() as chrome_driver:
        #get_basic_page_message(driver=chrome_driver)
        demo_click_element(driver=chrome_driver)
        #demo_scroll_page(driver=chrome_driver)
        #demo_keyboard_action(driver=chrome_driver)
        chrome_driver.quit()
