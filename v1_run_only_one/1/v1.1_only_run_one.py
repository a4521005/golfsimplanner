from contextlib import contextmanager
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

@contextmanager
def create_chrome_driver() -> Chrome:
    driver=Chrome()
    yield driver
    driver.quit()

#click
def demo_click_element(driver: Chrome):
    url = "https://golfsimplanner-stage.benq.com/?aspect_ratio=1%3A1&screen_size__golf_=7%278%22x7%278%22%20&launch_monitor_position=Top&recommended_projector=LW600ST"

    #Step1. 附件中, 開啟每個URL(還沒完成)
    Step1_Result=False
    driver.get(url)
    time.sleep(30)
    #document.querySelector("body")
    ##projector > div.v-overlay__content > div > div.v-card-text


    #I understand
    understand=False
    if understand==False:
        i_understand_button_element = "body > div.v-overlay-container > div.v-overlay.v-overlay--active.v-theme--light.v-locale--is-ltr.v-dialog.cookie-dialog > div.v-overlay__content > div > div.v-card-text.cookie-content > div.cookie-button-link-container > button"
        i_understand_button = driver.find_element(
            By.CSS_SELECTOR, i_understand_button_element
        )
        driver.execute_script(
            "arguments[0].click();",
            i_understand_button
        )
        time.sleep(10)
        understand = True

    #close
    close_button_element = "body > div.v-overlay-container > div:nth-child(16) > div.v-overlay__content > div > div.v-card-text > div.flex.justify-end > i"
    close_button = driver.find_element(
        By.CSS_SELECTOR, close_button_element
    )
    driver.execute_script(
        "arguments[0].click();",
        close_button
    )
    time.sleep(5)

    # close Product Card
    close_Product_Card_button_element = "#projector > div.v-overlay__content > div > div.v-card-text > div.image-div > i"
    close_Product_Card = driver.find_element(
        By.CSS_SELECTOR, close_Product_Card_button_element
    )
    driver.execute_script(
        "arguments[0].click();",
        close_Product_Card
    )
    time.sleep(5)

    #Step2. 在網頁中檢查是否該網頁的Model已經選擇了LW600ST
    Step2_Result="Fail"
    model_check="LW600ST"
    model_name_element="#control_bar > div:nth-child(8) > div:nth-child(1) > div.v-col.v-col-9 > div > div > div > div.v-field__field > div > div > span"
    model_name= driver.find_element(
        By.CSS_SELECTOR, model_name_element
    )
    print(model_name.text)
    if model_name.text==model_check:
        Step2_Result="Pass"

    print(Step2_Result)


    #檢查aspect_ratio: 得找到Aspect ratio這個Element, 然後一層層翻到v-select__selection-text裡面, 看看裡面的值是否正確, 比對位置在Excel的E欄位
    aspect_ratio_Result = "Fail"
    aspect_ratio_check = "1:1"
    aspect_ratio_element="#control_bar > div:nth-child(6) > div:nth-child(1) > div.v-col-sm-7.v-col-md-6.v-col-lg-5.v-col > div > div > div > div.v-field__field > div > div > span"
    aspect_ratio= driver.find_element(
        By.CSS_SELECTOR, aspect_ratio_element
    )
    print(aspect_ratio.text)
    if aspect_ratio.text==aspect_ratio_check:
        aspect_ratio_Result="Pass"

    print(aspect_ratio_Result)

    #檢查launch_monitor_position: 得找到Launch Monitor這個Element, 然後一層層翻到v-input__control裡面, 看看Property的outerText的值是否正確, 比對位置在Excel的I欄位
    launch_monitor_position_Result = "Fail"
    launch_monitor_position_check = "Top"
    launch_monitor_position_element="#control_bar > div:nth-child(2) > div:nth-child(2) > div.v-col-sm-7.v-col-md-6.v-col-lg-5.v-col > div > div > div > div.v-field__field > div > div > span"
    launch_monitor_position= driver.find_element(
        By.CSS_SELECTOR, launch_monitor_position_element
    )
    print(launch_monitor_position.text)
    if launch_monitor_position.text==launch_monitor_position_check:
        launch_monitor_position_Result="Pass"

    print(launch_monitor_position_Result)

    #檢查recommended_projector: 得找到Model這個Element, 然後一層層翻到v-select__selection-text裡面, 看看裡面的值是否正確, 比對位置在Excel的D欄位
    recommended_projector_Result = "Fail"
    recommended_projector_check = "LW600ST"
    recommended_projector_element="#control_bar > div:nth-child(8) > div:nth-child(1) > div.v-col.v-col-9 > div > div > div > div.v-field__field > div > div > span"
    recommended_projector= driver.find_element(
        By.CSS_SELECTOR, recommended_projector_element
    )
    print(recommended_projector.text)
    if recommended_projector.text==recommended_projector_check:
        recommended_projector_Result="Pass"

    print(recommended_projector_Result)

    #檢查screen_size__golf: 得找到Height這個Element, 然後一層層翻到v-field__input裡面, 看看Property裡面的value值是否正確, 比對位置在Excel的G欄位(會需要換算一下, 例如: 7’8”代表7呎8吋, =相當於7.7呎)
    screen_size_golf_Result = "Fail"
    screen_size_golf_check = "7.7"
    #screen_size_golf_element="#control_bar > div:nth-child(6) > div:nth-child(3) > div.v-col-sm-8.v-col-md-8.v-col-lg-8.v-col-xl-8.v-col > div > div.v-input__control > div > div.v-slider-thumb > div.v-slider-thumb__label-container > div > div"
    screen_size_golf_element="#input-30"
    screen_size_golf= driver.find_element(
        By.CSS_SELECTOR, screen_size_golf_element
    )
    print(screen_size_golf.get_property('value'))
    screen_size_golf_value=screen_size_golf.get_property('value')
    if screen_size_golf_value==screen_size_golf_check:
        screen_size_golf_Result="Pass"

    print(screen_size_golf_Result)

if __name__=='__main__':
    with create_chrome_driver() as chrome_driver:
        demo_click_element(driver=chrome_driver)
        #demo_scroll_page(driver=chrome_driver)
        #demo_keyboard_action(driver=chrome_driver)
        chrome_driver.quit()